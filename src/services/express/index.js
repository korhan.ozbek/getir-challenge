import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import helmet from 'helmet';

import config from '../../config';

const app = express();
app.use(helmet());
app.use(helmet.noCache());
app.use(helmet.permittedCrossDomainPolicies());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

if (config.ENV === 'development') {
    app.use(cors());
    app.use(morgan('dev'));
} else {
    app.use(morgan());
}

export default (routes) => {
    routes.forEach((routeItem) => {
        app.use(routeItem.path, routeItem.routes);
    });
    return app;
};
