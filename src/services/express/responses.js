export const success = (data) => ({
    code: 0,
    msg: 'Success',
    ...data,
});

export const unsuccess = (error) => ({
    code: 1,
    msg: 'Error',
    errors: error,
});
