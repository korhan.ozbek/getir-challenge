import mongoose from 'mongoose';

export default {
    connect: (uri) => {
        mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });
    },
};
