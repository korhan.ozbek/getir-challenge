import swaggerUi from 'swagger-ui-express';

import swaggerConfig from '../swagger.json';
import database from './services/mongoose';
import express from './services/express';
import config from './config';
import apiRoutes from './api';

database.connect(config.MONGO_URI);
const app = express([
    {
        path: '/api',
        routes: apiRoutes,
    },
]);

app.use('/apiDocs', swaggerUi.serve, swaggerUi.setup(swaggerConfig));

export default app;
