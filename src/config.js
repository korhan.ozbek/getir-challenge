export default {
    ENV: process.env.NODE_ENV || 'development',
    PORT: process.env.PORT || 3000,
    IP: process.env.IP || '0.0.0.0',
    API_PATH: process.env.API_PATH || '/api',
    MONGO_URI: process.env.MONGO_URI || 'mongodb://dbUser:dbPassword1@ds249623.mlab.com:49623/getir-case-study',
};
