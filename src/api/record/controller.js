import Record from './model';

export const getAll = ({ startDate, endDate }) => Record.find({
    createdAt: {
        $gte: new Date(startDate),
        $lt: new Date(endDate),
    },
});

export const getOne = ({ id }) => Record.find({
    _id: id,
});
