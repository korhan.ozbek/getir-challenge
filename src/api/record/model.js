import mongoose, { Schema } from 'mongoose';

const recordSchema = new Schema({
    key: {
        type: String,
    },
    value: {
        type: String,
    },
    createdAt: {
        type: Date,
    },
    counts: {
        type: [Number],
    },
});

const model = mongoose.model('Record', recordSchema);

export const { schema } = model;
export default model;
