import { Router } from 'express';
import { check, validationResult } from 'express-validator';

import { getAll, getOne } from './controller';
import { success, unsuccess } from '../../services/express/responses';

const router = new Router();

router.post('/', [
    check('startDate').isISO8601().toDate(),
    check('endDate').isISO8601().toDate(),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.json(unsuccess(errors.array()));
        return;
    }
    getAll(req.body)
        .then((records) => res.json(success({ records })))
        .catch((err) => res.json(unsuccess(err)));
});

router.post('/:id', (req, res) => getOne(req.params)
    .then((records) => res.json(success({ records })))
    .catch((err) => res.json(unsuccess(err))));

export default router;
