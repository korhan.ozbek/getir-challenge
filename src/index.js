import http from 'http';

import app from './app';
import config from './config';

const httpServer = http.createServer(app);

httpServer.listen(config.PORT, config.IP, () => {
    console.log('App listening on http://%s:%d', config.IP, config.PORT);
});
