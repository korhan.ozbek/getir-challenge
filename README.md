# Getir Challenge

This repository created for Getir Challenge. 

### Api Documentation
Api documentation is at the [https://korhanozbek.com/getirChallenge/apiDocs/](https://korhanozbek.com/getirChallenge/apiDocs/) address.

### Live Testing
The app working at  [https://korhanozbek.com/getirChallenge](https://korhanozbek.com/getirChallenge) address.