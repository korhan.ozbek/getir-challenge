import 'babel-polyfill';

import request from 'supertest';
import config from '../src/config';
import app from '../src/app';

const CORRECT_DATA = {
    REQUEST: {
        SUCCESS: {
            PARAMETER: { startDate: '2016-10-10', endDate: '2016-10-12' },
        },
    },
    RESULT: {
        SUCCESS: {
            CODE: 0,
            MSG: 'Success',
        },
        UNSUCCESS: {
            CODE: 1,
            MSG: 'Error',
        },
    },
};

const post = (url, body) => {
    const httpRequest = request(app).post(url);
    httpRequest.send(body);
    httpRequest.set('Accept', 'application/json');
    httpRequest.set('Origin', ('http://%s:%d', config.IP, config.PORT));
    return httpRequest;
};

describe('Get All Records', () => {
    it('succeeds with startDate and endDate', async () => {
        const response = await post('/api/records', CORRECT_DATA.REQUEST.SUCCESS.PARAMETER);
        expect(response.body.code).toBe(CORRECT_DATA.RESULT.SUCCESS.CODE);
        expect(response.body.msg).toBe(CORRECT_DATA.RESULT.SUCCESS.MSG);
    });
    it('fails with missing startDate and endDate parameters', async () => {
        const response = await post('/api/records');
        expect(response.body.code).toBe(CORRECT_DATA.RESULT.UNSUCCESS.CODE);
        expect(response.body.msg).toBe(CORRECT_DATA.RESULT.UNSUCCESS.MSG);
    });
});

describe('Sample Test', () => {
    it('should test that true === true', () => {
        expect(true).toBe(true);
    });
});
